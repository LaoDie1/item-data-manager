# ItemDataManager

> Godot 3.4.2

管理物品数据，鼠标指向物品图标查看物品的数据内容

![示例运行内容](https://img-blog.csdnimg.cn/55270c9a31b2474eb23b769cd02709a7.png)

连接这个节点的信号，通过信号添加设置节点和数据进行管理物品数据