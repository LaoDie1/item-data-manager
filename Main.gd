extends Node2D


const Scn_Item = preload("res://Item.tscn")
const Item = preload("res://Item.gd")


onready var n_item_manager = $ItemManager
onready var n_item_grid_container = find_node("ItemGridContainer")


## 物品数据对应的节点
var item_nodes := {}


func _ready():
	
	# 保存一个武器的 ID名称
	var temp_weapon_id : String = ""
	
	# 物品数据
	var item_data_path = "res://item_data.tres"
	var file = File.new()
	if file.file_exists(item_data_path):
		# 加载数据
		n_item_manager.load_data(item_data_path)
	
	# 没有保存则添加数据
	else:
		
		# 添加3个默认的武器： 武器A
		n_item_manager.add_items([
			ItemData.get_item_data("武器A"),
			ItemData.get_item_data("武器A"),
			ItemData.get_item_data("武器A"),
		])
		
		n_item_manager.add_items([
			# 添加 2 个 武器B
			ItemData.get_item_data("武器B"),
			ItemData.get_item_data("武器B"),
			# 添加 1 个 护甲A
			ItemData.get_item_data("护甲A"),
		])
		
		# 添加一个损耗 20% 的 武器A
		n_item_manager.add_item(
			ItemData.get_item_data("武器A"), 
			# 损耗 20%
			{
				ItemData.K.Loss: 20,
			}
		)
		
		# 添加一个损耗 50%，金币为 5 的 护甲A
		n_item_manager.add_item(
			ItemData.get_item_data("护甲A"), 
			# 损耗 50%，金币 5
			{
				ItemData.K.Loss: 50,
				ItemData.K.Coin: 5,
			}
		)
	
	if temp_weapon_id == "":
		var id_list := n_item_manager.get_all_item_data().keys() as Array
		if id_list.size() > 0:
			temp_weapon_id = id_list[0]
		print(n_item_manager.get_all_item_data())
	
	# 输出所有物品数据，可以看到 key 作为物品的 id 格式为：“物品名&序号”
	print_data("输出所有物品数据", n_item_manager.get_all_item_data())
	
	print_data('获取盔甲类型的物品', n_item_manager.get_item_by_type(ItemData.Type.Corselet))
	
	print_data("取出一个物品", n_item_manager.take_out(temp_weapon_id))
	
	yield(get_tree().create_timer(0.1), "timeout")
	print_data("取出后", n_item_manager.get_item_by_id(temp_weapon_id))
	
	
	# 修改武器数量为 4，然后保存
	var item = n_item_manager.get_item_by_id(temp_weapon_id)
	item[ItemData.K.Count] = 4
	
	# 保存数据
	n_item_manager.save_data(item_data_path)


## 输出数据
func print_data( title: String, data ):
	print("=".repeat(50))
	print(' [ %s ] ' % title)
	print("=".repeat(50))
	print(JSON.print(data, '\t'))



func _on_ItemManager_item_added(item: Dictionary, item_id: String):
	# 如果物品新添加了，则添加节点
	var n_item = Scn_Item.instance()
	
	n_item_grid_container.add_child(n_item)
	n_item.set_data(item)
	# 记录这个物品 id 对应的节点
	item_nodes[item_id] = n_item


func _on_ItemManager_item_changed(item, item_id):
	# 更新物品显示的信息
	if item_nodes.has(item_id):
		(item_nodes[item_id] as Item).update_data()


func _on_ItemManager_item_removed(item, item_id):
	# 移除这个物品的节点
	if item_nodes.has(item_id):
		(item_nodes[item_id] as Item).queue_free()


func _on_Remove_pressed():
	# 因为是以物品 Id名称 作为唯一标识作为 key 的
	# 所以通过这个可以获取所有物品的 id名称
	var id_list := n_item_manager.get_all_item_data().keys() as Array
	
	if id_list.size() > 0:
		# 移除物品
		var item_id := id_list[0] as String
		n_item_manager.remove_by_id(item_id)
	
	# 如果这是最后一个被移除了，则将 Remove 按钮节点的 不可用 属性设为 true
	if id_list.size() == 1:
		find_node("Remove").disabled = true
