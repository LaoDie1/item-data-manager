## 物品数据
class_name ItemData extends Reference


# 物品数据中的 key
const K = {
	Name = "name",		# 物品名
	Loss = "loss",		# 武器损耗
	Coin = "coin",		# 所需金币
	Type = "type",		# 物品类型
	Description = "desc",	# 描述
	Icon = "icon",			# 物品图片
	Count = "count",	# 物品数量
	
	Attack = "atk",	# 伤害
	Defence = "def",	# 防御
}

# 物品类型
const Type = {
	Normal = "normal",		# 普通物品
	Weapon = "weapon",		# 武器
	Corselet = "corselet",	# 护甲
}

const __data__ = {}


##  获取物品数据
## @item_name  物品名称
## @return  返回物品数据
static func get_item_data(item_name: String) -> Dictionary:
	if __data__.empty():
		for data in __item_data__():
			# 用物品的物品名作为 key
			__data__[data[K.Name]] = data
	# 通过物品的物品名获取物品数据
	return __data__[item_name].duplicate(true)


static func __item__(name: String, coin: int, description: String):
	return {
		K.Name: name,
		K.Coin: coin,
		K.Loss: 0,
		K.Description: description,
		K.Type: Type.Normal,
	}

# 武器
static func __weapon__(name: String, attack: float, coin : int, description: String = ""):
	var data = __item__(name, coin, description)
	data[K.Attack] = attack
	data[K.Type] = Type.Weapon
	data[K.Icon] = "res://icon.png"	# 不想麻烦再弄一些图片才这样写的..
	return data

# 盔甲
static func __corselet__(name: String, defence: float, coin : int, description: String = ""):
	var data = __item__(name, coin, description)
	data[K.Defence] = defence
	data[K.Type] = Type.Corselet
	data[K.Icon] = "res://icon.png"
	return data

static func __item_data__():
	return [
		# 武器
		__weapon__("武器A", 10, 20, "一把普通武器"),
		__weapon__("武器B", 20, 50, "精心打造的一把武器"),
		__weapon__("武器C", 50, 200, "精钢锻造的锋利武器"),
		__weapon__("武器D", 100, 500, "金刚石打造的超级耐用的武器"),
		
		# 盔甲
		__corselet__("护甲A", 6, 30, "普通的盔甲"),
		__corselet__("护甲B", 15, 80, "比较厚重的盔甲，可抵抗更强力的攻击"),
	]


"""
以上是为了减少代码的书写，也直接删了上面的方法名前后缀为两个下划线 __ 的方法
改为直接在 __data__ 里写：

const __data__ = {
	"武器A": {
		K.Name: "武器A",
		K.Attack: 10,
		K.Loss: 0,
		K.Coin: 20,
		K.Type: Type.Weapon,
		K.Description: "一把普通武器"
	},
	"武器B": {
		K.Name: "武器B",
		K.Attack: 20,
		K.Loss: 0,
		K.Coin: 50,
		K.Type: Type.Weapon,
		K.Description: "精心打造的一把武器"
	},
	
	（下面都是这个格式，所以省略...）
}

或者直接读取这种 json 文件

"""


