extends TextureRect


var __data__ : Dictionary

onready var __n_label__ = $Label


func set_data(value: Dictionary):
	__data__ = value
	texture = load(__data__[ItemData.K.Icon])
	hint_tooltip = JSON.print(__data__, '\t')
	if __n_label__ == null:
		yield(self, "ready")
	__n_label__.text = str(__data__[ItemData.K.Count])


func update_data():
	__n_label__.text = str(__data__[ItemData.K.Count])


